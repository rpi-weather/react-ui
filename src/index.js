import React from 'react'
import {render} from 'react-dom'
import MainView from './components/main-view.js'
// require('style!css!./stylesheets/base.css');
import './stylesheets/base.css'

class App extends React.Component {
  render () {
    return <MainView className = "container-main-view" />
  }
}

render(<App/>, document.getElementById('app'))
