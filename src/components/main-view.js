import React from 'react'
import SwipeableViews from 'react-swipeable-views'
import TodayEventList from '../components/events/today-event-list.js'
import FutureEventList from '../components/events/future-event-list.js'
import Clock from '../components/clock/local-clock.js'
import WorldClock from '../components/clock/world-clock.js'
import TodayDate from '../components/clock/today-date.js'
import TodayWeather from '../components/weather/today-weather.js'
import HourlyWeather from '../components/weather/hourly-weather.js'
import DailyWeather from '../components/weather/daily-weather.js'
import WeatherAlerts from '../components/weather/weather-alerts.js'
// import SimpleMap from '../components/google-map.js';
import Legend from '../components/legend.js'

const styles = {
  slide: {
    padding: 20,
    minHeight: 480,
    minWidth: 800,
    background: 'none'
  }
}

const MainView = () => (
  <SwipeableViews>
    <div style={styles.slide}>
      <div className = "date-time-container">
        <Clock />
      </div>
      <TodayDate />
      <WorldClock />
      <div className = "weather-events-container">
        <TodayWeather />
        <TodayEventList />
      </div>
    </div>
    <div style={styles.slide}>
      <div className = "weather-view">
        <div className = "weather-view-overview-alerts">
          <TodayWeather />
          <WeatherAlerts />
        </div>
        <HourlyWeather />
        <DailyWeather />
      </div>
    </div>
  </SwipeableViews>
)

export default MainView
