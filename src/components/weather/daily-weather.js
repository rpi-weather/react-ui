import React from 'react'
import moment from 'moment'
import axios from 'axios'
import _ from 'underscore'
import {mapIcon} from '../../helper.js'

import '../../stylesheets/weather.css'

class DailyWeather extends React.Component {
  constructor(props, context){
    super(props, context)
    this.state = { secondsElapsed: 0 }
  }

  componentWillMount () {
    let _this = this
    axios.get(document.location + 'daily-forecast')
         .then(response => {
           _this.setState(response.data)
           return
    })
  }

  componentDidMount () {
    this.interval = setInterval(() => this.tick(), 7.2e+6)
  }
  componentWillUnmount () {
    clearInterval(this.interval)
  }

  tick () {
    this.setState({
      secondsElapsed: this.state.secondsElapsed + 1
    })
  }

  render () {
    return (
      <div className="weather-daily-forecast-container">
      {
        _.map(this.state.dailyForecast, function(dayWeather) {
          return (
            <div key={dayWeather.date.epoch}>
              <div>{dayWeather.low.celsius}°/{dayWeather.high.celsius}°</div>
              <div className="icon" data-icon={mapIcon(dayWeather.icon_url)} />
              <div className="forecastHour">{dayWeather.date.weekday}</div>
            </div>
          )
        })
      }
    </div>
    )
  }
}

export default DailyWeather
