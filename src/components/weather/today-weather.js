import React from 'react'
import moment from 'moment'
import axios from 'axios'
import AirQuality from './air-quality.js'
import '../../stylesheets/weather.css'

class TodayWeather extends React.Component {
  constructor(props, context){
    super(props, context)
    this.state = {}
  }

  componentWillMount () {
    let _this = this
    axios.get(document.location + 'conditions-today')
         .then(response => {
           _this.setState(response.data)
           return
    })
  }

  render () {
    return (
      <div className="today-weather-container">
        <div className="weather-day">
          <div className="weather-now">
            <div className="weather-now-icon">
              <div className="icon" data-icon={this.state.iconName}></div>
            </div>
            <div className="weather-now-degree">{this.state.temperature}<span>°</span></div>
            <div className="weather-now-feels-like">Feels like <span>{this.state.feelslike}</span>°</div>
            <AirQuality />
          </div>
        </div>
      </div>
  )
  }
}

export default TodayWeather
