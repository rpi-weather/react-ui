import React from 'react'
import axios from 'axios'

import '../../stylesheets/weather.css'

class AirQuality extends React.Component {
  constructor(props, context){
    super(props, context)
    this.state = { secondsElapsed: 0 }
  }

  componentWillMount () {
    let _this = this
    axios.get(document.location + 'air-quality')
         .then(response => {
           _this.setState(response.data)
           return
    })
  }

  componentDidMount () {
    this.interval = setInterval(() => this.tick(), 7.2e+6)
  }
  componentWillUnmount () {
    clearInterval(this.interval)
  }

  tick () {
    this.setState({
      secondsElapsed: this.state.secondsElapsed + 1
    })
  }

  render () {
    return <div className="air-quality">Air quality: {this.state.airQuality}</div>
  }
}

export default AirQuality
