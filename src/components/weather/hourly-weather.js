import React from 'react'
import moment from 'moment'
import axios from 'axios'
import _ from 'underscore'
import {mapIcon} from '../../helper.js'

import '../../stylesheets/weather.css'

class HourlyWeather extends React.Component {
  constructor(props, context){
    super(props, context)
    this.state = { secondsElapsed: 0 }
  }

  componentWillMount () {
    let _this = this
    axios.get(document.location + 'hourly-forecast')
         .then(response => {
           _this.setState(response.data)
           return
    })
  }

  componentDidMount () {
    this.interval = setInterval(() => this.tick(), 900000)
  }
  componentWillUnmount () {
    clearInterval(this.interval)
  }

  tick () {
    this.setState({
      secondsElapsed: this.state.secondsElapsed + 1
    })
  }

  render () {
    return (
      <div className="weather-now-hourly">
      {
        _.map(this.state.hourlyForecast, function(hourWeather) {
          return (
            <div key={hourWeather.FCTTIME.epoch}>
              <div>{hourWeather.temp.metric}°</div>
              <div className="icon" data-icon={mapIcon(hourWeather.icon_url)} />
              <div className="forecastHour">{hourWeather.FCTTIME.civil.replace(/:00/,'')}</div>
            </div>
          )
        })
      }
    </div>
    )
  }
}

export default HourlyWeather
