import React from 'react'
import moment from 'moment'
import axios from 'axios'
import _ from 'underscore'

import '../../stylesheets/weather.css'

class WeatherAlerts extends React.Component {
  constructor(props, context){
    super(props, context)
    this.state = {}
  }

  componentWillMount () {
    let _this = this
    axios.get(document.location + 'weather-alerts')
         .then(response => {
           _this.setState(response.data)
           return
    })
  }

  render () {
    return (
      <div className="weather-alerts-container">
      {
        _.map(this.state.alerts, function(alert) {
          return (
            <div>
              <div>{moment(alert.date).format('LL')}</div>
              <div>{alert.message}</div>
              <div>{alert.description}</div>
            </div>
          )
        })
      }
    </div>
    )
  }
}

export default WeatherAlerts
