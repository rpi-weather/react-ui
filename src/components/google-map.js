import React, { Component } from 'react'
import Map from 'google-maps-react'

class SimpleMap extends Component {
  render() {
    let center = {lat: 45.525811, lng: -73.6109403}
    return (
      <Map center={center} google={window.google} zoom={12} />
    )
  }
}

export default SimpleMap
