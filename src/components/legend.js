import React from 'react'
import _ from 'underscore'
import {arrayIconDayWeatherMapping} from '../helper.js'
import '../stylesheets/legend.css'
import '../stylesheets/weather.css'

class Legend extends React.Component {
  render() {
    return (
        <div className="list-legend">
          {
            _.map(arrayIconDayWeatherMapping, function (value, key, obj) {
              return (
                <div>
                  <div className="title-legend">{key}</div>
                  <div className="icon icon-legend" data-icon={obj[key]}></div>
                </div>
              )
            })
          }
        </div>
      )
  }
}

export default Legend
