import React from 'react'
import moment from 'moment'
// require('style!css!../../stylesheets/clock.css');
import '../../stylesheets/clock.css'

class TodayDate extends React.Component {
  render () {
    return <div className = "today-date" >{moment().format('LL')}</div>
  }
}

export default TodayDate
