import React from 'react'
import moment from 'moment'
// require('style!css!../../stylesheets/clock.css');
import '../../stylesheets/clock.css'

class Clock extends React.Component {
  constructor(props, context){
    super(props, context)
    this.state = {date: new Date()}
  }

  componentDidMount() {
    this.interval = setInterval(() => this.tick(), 1000)
  }

  componentWillUnmount() {
    clearInterval(this.interval)
  }

  tick () {
    this.setState({
      date: new Date()
    })
  }

  render () {
    return <div className = "local-time" >{moment(this.state.date).format('h:mm')}</div>
  }
}

export default Clock
