import React from 'react'
import moment from 'moment-timezone'
// require('style!css!../../stylesheets/clock.css');
import '../../stylesheets/clock.css'

class WorldClock extends React.Component {
  constructor(props, context){
    super(props, context)
    this.state = {dateParis: new Date(), dateTokyo: new Date()}
  }

  componentDidMount() {
    this.interval = setInterval(() => this.tick(), 1000)
  }

  componentWillUnmount() {
    clearInterval(this.interval)
  }

  tick () {
    this.setState({
      dateParis: new Date(),
      dateTokyo: new Date()
    })
  }

  render () {
    return (
      <div className = "world-time">
        <div>Paris <div>{moment.tz(moment(this.state.dateParis), "Europe/Paris").format('LT')}</div></div>
        <div>Tokyo <div>{moment.tz(moment(this.state.dateTokyo), "Asia/Tokyo").format('LT')}</div></div>
      </div>
    )
  }
}

export default WorldClock
