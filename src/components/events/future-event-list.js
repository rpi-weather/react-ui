import _ from 'underscore'
import React from 'react'
import axios from 'axios'
import EventItem from './event-item'

class FutureEventList extends React.Component {
  constructor(props, context){
    super(props, context)
    this.state = { events: [] }
  }

  componentWillMount () {
     let _this = this
     axios.get(document.location + 'future-events')
       .then(response => {
         _this.setState({
           events: response.data.events
         })
         return response.data.events
       })
  }

  render () {
    return (
      <div className="future-event-list-container">
        <ul>
          {
            _.map(this.state.events, function(event){
              return (<EventItem key={event.etag} event = {event}/>)
            })
          }
          </ul>
      </div>
      )
  }
}

export default FutureEventList
