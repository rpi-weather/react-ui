import React from 'react';
import moment from 'moment';

class EventItem extends React.Component {
  iscurrentDate (date){
    if (moment(date).isSame(moment(), "day")){
      return true;
    }
    return false;
  }
  displayCurrentDate(event){
    let displayedDateTime = event.start.dateTime || event.start.date;
    if (moment(displayedDateTime).isSame(moment(), "day")){
      return moment(displayedDateTime).format('h:mm');
    }
    return moment(displayedDateTime).format('MMMM Do, h:mm');
  }
  render () {

    let event = this.props.event;
    console.log(event);
    return (<li>{this.displayCurrentDate(event) + ' ' + event.summary}</li>);
  }
}

export default EventItem;
