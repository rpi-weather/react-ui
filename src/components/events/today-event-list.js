import _ from 'underscore'
import React from 'react'
import axios from 'axios'
import EventItem from './event-item'
import '../../stylesheets/events.css'

class TodayEventList extends React.Component {
  constructor(props, context){
    super(props, context)
    this.state = { events: [] }
  }

  componentWillMount () {
     let _this = this
     axios.get(document.location + 'today-events')
       .then(response => {
         _this.setState({
           events: response.data.events
         })
         return response.data.events
       })
  }

  render () {
    return (
      <div className = "today-event-list-container">
        <ul>
        {
          _.map(this.state.events, function(event){
            return (<EventItem key={event.start.dateTime} event = {event}/>)
          })
        }
        </ul>
      </div>)
  }
}

export default TodayEventList
