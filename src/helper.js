export const arrayIconDayWeatherMapping = {
  "chanceflurries": "X",
  "chancerain": "Q",
  "chancesnow": "V",
  "chancestorms": "U",
  "clear": "B",
  "cloudy": "N",
  "flurries": "R",
  "fog": "Y",
  "hazy": "J",
  "mostlycloudy": "L",
  "partlycloudy": "N",
  "mostlysunny": "H",
  "sunny": "B",
  "sleet": "X",
  "rain": "R",
  "snow": "W",
  "tsnorms": "P",
  "nt_chanceflurries": "$",
  "nt_chancerain": "7",
  "nt_chancesnow": '"',
  "nt_chancestorms": "6",
  "nt_clear": "C",
  "nt_cloudy": "4",
  "nt_flurries": "8",
  "nt_fog": "%",
  "nt_hazy": "K",
  "nt_mostlycloudy": "L",
  "nt_partlycloudy": "4",
  "nt_mostlysunny": "4",
  "nt_sunny": "C",
  "nt_sleet": "$",
  "nt_rain": "8",
  "nt_snow": "#",
  "nt_tsnorms": "&"
};

export function mapIcon(iconUrl) {
  let arrUrl = iconUrl.split('/');
  let icon = arrayIconDayWeatherMapping[arrUrl[arrUrl.length-1].split('.')[0]];
  if(icon === undefined){ icon = 'C'; }
  return icon;
}
