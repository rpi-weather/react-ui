FROM registry.gitlab.com/node-qemu/multiarch-nodejs

# ENV GOOGLE_CAL_CREDENTIALS
# ENV GOOGLE_CAL_ACCOUNT
# ENV GOOGLE_CAL_URL
# ENV AIR_QUALITY_TOKEN
# ENV WUNDERGROUND_TOKEN

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm set npm_config_arch arm && \
    npm --arch=arm install

COPY . .

RUN npm run build

RUN npm prune --production

RUN mv /usr/src/app/dist/ /usr/src/

WORKDIR /usr/src/dist

RUN mv /usr/src/app/node_modules /usr/src/dist

RUN rm -rf /usr/src/app

EXPOSE 3000

CMD [ "node", "tools/srcServer.js" ]
