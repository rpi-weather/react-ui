let credentials = (process.env.GOOGLE_CAL_CREDENTIALS)
// import fs from 'fs'
// let TOKEN_PATH = '/Users/clemence/dev/webview-raspberry/calendar-access-ad71c199606d.json'

// read private key file
// let json = fs.readFileSync(TOKEN_PATH, 'utf8')
// let credentials = JSON.parse(json)

module.exports.key = credentials.private_key

const CALENDAR_ID = {
  'primary': process.env.GOOGLE_CAL_ACCOUNT
}
const SERVICE_ACCT_ID = credentials.client_email
const CALENDAR_URL = process.env.GOOGLE_CAL_URL
module.exports.calendarId = CALENDAR_ID
module.exports.serviceAcctId = SERVICE_ACCT_ID
module.exports.calendarUrl = CALENDAR_URL
