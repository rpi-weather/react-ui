import express from 'express'
import path from 'path'

/* eslint-disable no-console */

const port = 3000
const app = express()

require('./wunderground-api.js')(app)
require('./googlecal-api.js')(app)
require('./air-quality-api.js')(app)

app.use(express.static(path.join( __dirname, '../src/')))

app.listen(port, function(err) {
  if (err) {
    console.log(err)
  } else {
    console.log("Server started on port "+port)
  }
})
