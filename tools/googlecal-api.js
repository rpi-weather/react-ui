import CalendarAPI from 'node-google-calendar'
import moment from 'moment'
import _ from 'underscore'
const GOOGLE_CAL_CONFIG = require('./config/google-cal-settings.js')
const cal = new CalendarAPI(GOOGLE_CAL_CONFIG)

function serializedEvents(json){
  let events = []
  _.map(json, function(event){
    if (typeof(event.reccurrence) == 'undefined' && typeof(event.start) != 'undefined'){
      console.log(event.reccurrence)
      events.push(
        {
          summary: event.summary,
          start: event.start,
          end: event.end
        }
      )
    }
  })
  events = _.sortBy(events, 'dateTime')
  return events
}

module.exports = function(app){
  app.get('/today-events', function(req, res) {
    console.log('#get today-events...')
    let startDate = moment().startOf('day').format()
    let endDate = moment().endOf('day').format()
    cal.listEvents(GOOGLE_CAL_CONFIG.calendarId['primary'], startDate, endDate, {singleEvents: true}).then(function(json){
        res.status(200).json({ events: serializedEvents(json) })
     }, function (json){
        //Error
        console.log("list events error : " + json)
     })
  })

  app.get('/future-events', function(req, res) {
    console.log('#get future-events...')
    let startDate = moment().format()
    let endDate = moment().add(8, 'days').format()
    cal.listEvents(GOOGLE_CAL_CONFIG.calendarId['primary'], startDate, endDate, {singleEvents: true}).then(function(json){
        // console.log(serializedEvents(json));
        res.status(200).json({ events: serializedEvents(json) })
     }, function (json){
        //Error
        console.log("list events error : " + json)
     })
  })
}
