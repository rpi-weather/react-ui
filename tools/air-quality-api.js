const AIR_QUALITY_CONFIG = require('./config/air-quality-settings.js')
const airQualityUrl = 'http://api.waqi.info/feed/montreal/?token=' + AIR_QUALITY_CONFIG.appToken
import axios from 'axios'

module.exports = function(app){
  app.get('/air-quality', function(req, res) {
    console.log('#get air quality...')
    axios.get(airQualityUrl).then(function (response){
      let serializedData = {
        airQuality: statusAirQuality(response.data.aqi)
      }

      res.status(200).json(serializedData)
    }).catch(function (error) {
      console.log(error)
    })
  })
}

function statusAirQuality(aqi){
  if(aqi >= 51){
    return 'Acceptable'
  }else if (aqi >= 26) {
    return 'Bad'
  }
  return 'Good'
}
