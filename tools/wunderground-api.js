const WUNDERGROUND_CONFIG = require('./config/wunderground-settings.js')
const weatherConditionsUrl = 'http://api.wunderground.com/api/' + WUNDERGROUND_CONFIG.appToken + '/conditions/q/Canada/Montreal.json'
const weatherHourlyForecastUrl = 'http://api.wunderground.com/api/' + WUNDERGROUND_CONFIG.appToken + '/hourly/q/Canada/Montreal.json'
const weatherDaysForecastUrl = 'http://api.wunderground.com/api/' + WUNDERGROUND_CONFIG.appToken + '/forecast10day/q/Canada/Montreal.json'
const weatherAlertsUrl = 'http://api.wunderground.com/api/' + WUNDERGROUND_CONFIG.appToken + '/alerts/q/Canada/Montreal.json'
import axios from 'axios'
import {mapIcon} from '../src/helper.js'

module.exports = function(app){
  app.get('/conditions-today', function(req, res){
    console.log('#get today weather...')
    axios.get(weatherConditionsUrl).then(function (response){
      let serializedData = {
        temperature: response.data.current_observation.temp_c,
        feelslike: response.data.current_observation.feelslike_c,
        iconName: mapIcon(response.data.current_observation.icon_url),
        summary: response.data.current_observation.weather,
        humidity: response.data.current_observation.relative_humidity,
        wind: response.data.current_observation.wind_kph,
        uvindex: response.data.current_observation.UV
      }
      res.status(200).json(serializedData)
    }).catch(function (error) {
      console.log(error)
    })
  })

  app.get('/hourly-forecast', function(req, res){
    console.log('#get today hourly forecast weather...')
    axios.get(weatherHourlyForecastUrl).then(function (response){
      let serializedData = {
        hourlyForecast: response.data.hourly_forecast
      }

      res.status(200).json(serializedData)
    }).catch(function (error) {
      console.log(error)
    })
  })

  app.get('/daily-forecast', function(req, res){
    console.log('#get weather week forecast...')
    axios.get(weatherDaysForecastUrl).then(function (response){
      let serializedData = {
        dailyForecast: response.data.forecast.simpleforecast.forecastday
      }
      res.status(200).json(serializedData)
    }).catch(function (error) {
      console.log(error)
    })
  })

  app.get('/weather-alerts', function(req, res){
    console.log('#get weather alerts...')
    axios.get(weatherAlertsUrl).then(function (response){
      let serializedData = {
        alerts: response.data.alerts
      }
      res.status(200).json(serializedData)
    }).catch(function (error) {
      console.log(error)
    })
  })
}
